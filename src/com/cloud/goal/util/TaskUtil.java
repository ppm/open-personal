package com.cloud.goal.util;

public class TaskUtil {

	/* task statuses */
	public static final int TASK_STATUS_SAVE = 1;
	public static final int TASK_STATUS_PUBLISH = 2;
	public static final int TASK_STATUS_FINISH = 3;
	public static final int TASK_STATUS_PAUSE = 4;
	public static final int TASK_STATUS_STOP = 5;
	
	/* task lights */
	public static final int TASK_LIGHT_NONE = 0;
	public static final int TASK_LIGNT_GREEN = 1;
	public static final int TASK_LIGHT_YELLOW = 2;
	public static final int TASK_LIGHT_RED = 3;
	public static final int TASK_LIGHT_GRAY = 4;
}
