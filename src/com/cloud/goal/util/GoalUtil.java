package com.cloud.goal.util;

public class GoalUtil {

	public static final int GOAL_TYPE_FUTURE = 1;
	public static final int GOAL_TYPE_LONG = 2;
	public static final int GOAL_TYPE_SHORT = 3;
	
	public static final int GOAL_PRIORITY_HIGH = 1;
	public static final int GOAL_PRIORITY_MIDDLE = 2;
	public static final int GOAL_PRIORITY_LOW = 3;
}
