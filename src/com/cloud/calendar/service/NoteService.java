package com.cloud.calendar.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloud.calendar.model.Note;
import com.cloud.platform.IDao;
import com.cloud.platform.util.Constants;
import com.cloud.platform.util.StringUtil;

@Component
public class NoteService {

	@Autowired
	private IDao dao;
	
	/**
	 * search user's all notes
	 * 
	 * @param userId
	 * @return
	 */
	public List<Note> searchNotes(String userId) {
		
		if(StringUtil.isNullOrEmpty(userId)) {
			return new ArrayList();
		}
		
		// search all notes for specific user
		String hql = "from Note where uid = ?";
		
		return dao.getAllByHql(hql, userId);
	}
	
	/**
	 * save note
	 * 
	 * @param userId
	 * @param text
	 * @param left
	 * @param top
	 * @param color
	 */
	public String saveNote(String userId, String text, String left, String top,
			String color, String size) {
		
		Note n = new Note();
		
		n.setId(Constants.getID());
		n.setUid(userId);
		n.setContent(text);
		n.setOffsetX(Integer.parseInt(left));
		n.setOffsetY(Integer.parseInt(top));
		n.setColor(color);
		n.setSize(size);
		
		// save new note
		dao.saveObject(n);
		
		return n.getId();
	}
	
	/**
	 * update note's position when move note
	 * 
	 * @param id
	 * @param left
	 * @param top
	 */
	public void updateNote(String id, String left, String top) {
		
		if(StringUtil.isNullOrEmpty(id)) {
			return;
		}
		
		Note n = (Note) dao.getObject(Note.class, id);
		
		n.setOffsetX(Integer.parseInt(left));
		n.setOffsetY(Integer.parseInt(top));
		
		dao.saveObject(n);
	}
	
	/**
	 * remove note
	 * 
	 * @param id
	 */
	public void removeNote(String id) {
		
		if(StringUtil.isNullOrEmpty(id)) {
			return;
		}
		
		dao.removeById(Note.class, id);
	}

	public IDao getDao() {
		return dao;
	}

	public void setDao(IDao dao) {
		this.dao = dao;
	}
}
