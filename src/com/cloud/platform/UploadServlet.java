package com.cloud.platform;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class UploadServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		doPost(req, resp);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		// init upload
		FileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
		
		upload.setHeaderEncoding("UTF-8");
		
		// init dest store dir
		String basePath = req.getRealPath("/") + "upload\\";
		
		try {
			// get upload files
			List items = upload.parseRequest(req);
			
			for(Object o : items) {
				// get upload file
				FileItem file = (FileItem) o;
				
				// get dest store file address
				String storeFile = basePath + file.getName();
				
				// store upload file
				file.write(new File(storeFile));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
