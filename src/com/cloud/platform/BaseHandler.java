package com.cloud.platform;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.cloud.platform.util.Constants;

public class BaseHandler {
	
	public HttpServletRequest getRequest(){
		return (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
	}
	
	public HttpSession getSession() {
		return getRequest().getSession();
	}
	
	public String getParameter(String name){
		return getRequest().getParameter(name);
	}
	
	public String getLoginUserId() {
		return (String) getSession().getAttribute(Constants.LOGIN_USER);
	}
}
