package com.cloud.platform.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class GoalTypeConverter implements Converter {

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		String type = "";
		value = value.toString();
		
		if("1".equals(value)) {
			type = "梦想目标";
		} else if("2".equals(value)) {
			type = "长期目标";
		} else if("3".equals(value)) {
			type = "短期目标";
		}
		
		return type;
	}
	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		return null;
	}

}
