package com.cloud.knowledge.web;

import java.util.List;

import com.cloud.knowledge.model.Knowledge;
import com.cloud.knowledge.service.KnowledgeService;
import com.cloud.platform.BaseHandler;

public class KnowledgeBean extends BaseHandler {

	private KnowledgeService knowledgeService;
	
	private List<Knowledge> knowledges;
	
	public void save() {
		try {
			String userId = getLoginUserId();
			String info = getParameter("knowledgeInfo");
			
			knowledgeService.saveKnowledgeInfo(userId, info);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public List<Knowledge> getKnowledges() {
		if(knowledges == null) {
			knowledges = knowledgeService.searchKnowledges(getLoginUserId());
		}
		return knowledges;
	}

	/**
	 * =======================  getters and setters  =======================
	 */
	public void setKnowledges(List<Knowledge> knowledges) {
		this.knowledges = knowledges;
	}

	public KnowledgeService getKnowledgeService() {
		return knowledgeService;
	}

	public void setKnowledgeService(KnowledgeService knowledgeService) {
		this.knowledgeService = knowledgeService;
	}
}
