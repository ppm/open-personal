package com.cloud.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloud.app.model.AppInfo;
import com.cloud.app.util.AppUtil;
import com.cloud.platform.IDao;
import com.cloud.platform.util.Constants;
import com.cloud.platform.util.StringUtil;

@Component
public class AppService {

	@Autowired
	private IDao dao;
	
	/**
	 * get app course info
	 * 
	 * @param loginUser
	 * @return
	 */
	public String getCourseInfo(String loginUser) {
		
		String info = "";
		
		String hql = "select info from AppInfo where uid = ? and type = ?";
		List list = dao.getAllByHql(hql, new Object[] {loginUser, AppUtil.APP_TYPE_COURSE});
		
		if(!list.isEmpty()) {
			info = (String) list.get(0);
		}
		
		return info;
	}
	
	/**
	 * save course info
	 * 
	 * @param courseInfo
	 */
	public void saveCourse(String courseInfo, String loginUser) {
		
		if(StringUtil.isNullOrEmpty(courseInfo)) {
			return;
		}
		
		// save app course info
		String hql = "from AppInfo where uid = ? and type = ?";
		List list = dao.getAllByHql(hql, new Object[] {loginUser, AppUtil.APP_TYPE_COURSE});
		
		AppInfo course = null;
		
		if(list.isEmpty()) {
			course = new AppInfo();
			course.setId(Constants.getID());
			course.setUid(loginUser);
			course.setType(AppUtil.APP_TYPE_COURSE);
		} else {
			course = (AppInfo) list.get(0);
		}
		
		course.setInfo(courseInfo);
		
		dao.saveObject(course);
	}
}
