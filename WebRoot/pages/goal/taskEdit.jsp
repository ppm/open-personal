<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/json2.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="saveTask();return false;">保存</a>
		<a href="#" onclick="clearPage('taskFrm');return false;">返回</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">任务名称</td>
				<td class="edit-whole" colspan="3">
					<h:inputText id="name" value="#{taskBean.task.name}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">优先级</td>
				<td class="edit-whole" colspan="3">
					<div id="priority" class="select-div">
						<ul class="menu-ul">
							<li>请选择</li>
							<li val="1">高</li>
							<li val="2">中</li>
							<li val="3">低</li>
						</ul>
					</div>
				</td>
			</tr>
			<tr>
				<td class="edit-label">起始日期</td>
				<td class="edit-whole" colspan="3">
					<input id="startDate" name="startDate" class="edit-timer" value="<h:outputText value="#{taskBean.task.startDate}" converter="DateConverter" />" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">结束日期</td>
				<td class="edit-whole" colspan="3">
					<input id="endDate" name="endDate" class="edit-timer" value="<h:outputText value="#{taskBean.task.endDate}" converter="DateConverter" />" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">任务描述 </td>
				<td class="edit-whole" colspan="3">
					<h:inputTextarea id="remark" value="#{taskBean.task.remark}" style="height: 160px;" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">验收标准</td>
				<td class="edit-whole" colspan="3">
					<h:inputTextarea id="acceptance" value="#{taskBean.task.acceptance}" style="height: 160px;" />
				</td>
			</tr>
		</table>
		
		<input type="hidden" id="_info" name="taskInfo" />
		<h:inputHidden id="taskId" value="#{taskBean.task.id}" />
		<h:inputHidden id="priority" value="#{taskBean.task.priority}" />
		
		<a4j:commandLink id="save" action="#{taskBean.saveTask}" oncomplete="saveComplete();" />
	</h:form>
	
	<script>
		var info;
		$("#startDate, #endDate").omCalendar();
		$("#priority").select().selectVal($("#operate\\:priority").val());
		
		function saveTask() {
			info = {};
			info.i = $("#operate\\:taskId").val();
			info.n = $("#operate\\:name").val();
			info.p = $("#priority").selectVal();
			info.pn = $("#priority").selectText();
			info.s = $("#startDate").val();
			info.e = $("#endDate").val();
			info.r = $("#operate\\:remark").val();
			info.a = $("#operate\\:acceptance").val();
			
			$("#_info").val(JSON.stringify(info));
			$("#operate\\:save").click();
		}
		
		function saveComplete() {
			$("#tasksFrm", parent.document)[0].contentWindow.refresh(info);
			clearPage("taskFrm");
		}
	</script>
</body>
</html>
</f:view>