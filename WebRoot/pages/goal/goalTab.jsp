<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-panel.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-tabs.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="clearPage('goalFrm');return false;">返回</a>
	</div>
	
	<div style="padding: 6px 0 6px 10px;color: red;border-bottom: 1px solid #999;">
		<h:outputText value="#{goalBean.goal.name}" />
	</div>
	
	<div id="tabs">
		<ul>
			<li><a id="1" href="#infoTab">基本信息</a></li>
			<li><a id="2" href="#planTab">计划管理</a></li>
			<li><a id="2" href="#statTab">目标仪表盘</a></li>
		</ul>
		<div id="infoTab" style="padding: 0;">
			<iframe src="" width="100%" frameBorder="0"></iframe>
		</div>
		<div id="planTab" style="padding: 0;">
			<iframe src="<c:url value="/pages/goal/planList.jsf?goalId=${param.goalId}" />" width="100%" height="481px" frameBorder="0"></iframe>
		</div>
		<div id="statTab" style="padding: 0;">
			<iframe src="" width="100%" frameBorder="0"></iframe>
		</div>
	</div>
	
	<script>
		$(function() {
			$("#tabs").omTabs({
				border: 0,
				active: 1
			});
		});
		
		function refresh() {
			var selId = $("#tabs").omTabs("getActivated");
			var tab = $($("#" + selId).attr("href"));
			var iframe = $("iframe", tab);
			
			if(typeof iframe[0].contentWindow.refresh == "function") {
				iframe[0].contentWindow.refresh();
			}
		}
	</script>
</body>
</html>
</f:view>