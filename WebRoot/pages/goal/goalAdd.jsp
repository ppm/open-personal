<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="saveGoal();return false;">保存</a>
		<a href="#" onclick="clearPage('goalFrm');return false;">返回</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">目标名称</td>
				<td class="edit-whole" colspan="3">
					<h:inputText value="#{goalBean.goal.name}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">起始日期</td>
				<td class="edit-whole" colspan="3" style="padding-left: 3px;">
					<input id="startDate" name="startDate" class="edit-timer" value="<h:outputText value="#{goalBean.goal.startDate}" converter="DateConverter" />" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">结束日期</td>
				<td class="edit-whole" colspan="3" style="padding-left: 3px;">
					<input id="endDate" name="endDate" class="edit-timer" value="<h:outputText value="#{goalBean.goal.endDate}" converter="DateConverter" />" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">目标类型</td>
				<td class="edit-whole" colspan="3">
					<div id="type" class="select-div">
						<ul class="menu-ul">
							<li>请选择</li>
							<li val="1">梦想目标</li>
							<li val="2">长期目标</li>
							<li val="3">短期目标</li>
						</ul>
					</div>
				</td>
			</tr>
			<tr>
				<td class="edit-label">优先级</td>
				<td class="edit-whole" colspan="3">
					<div id="priority" class="select-div">
						<ul class="menu-ul">
							<li>请选择</li>
							<li val="1">高</li>
							<li val="2">中</li>
							<li val="3">低</li>
						</ul>
					</div>
				</td>
			</tr>
			<tr>
				<td class="edit-label">描述 </td>
				<td class="edit-whole" colspan="3">
					<h:inputTextarea value="#{goalBean.goal.remark}" style="height: 160px;" />
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="goalId" value="${param.goalId}" />
		<h:inputHidden id="goalId" value="#{goalBean.goal.id}" />
		
		<h:inputHidden id="type" value="#{goalBean.goal.type}" />
		<h:inputHidden id="priority" value="#{goalBean.goal.priority}" />
		
		<a4j:commandLink id="save" action="#{goalBean.saveGoal}" oncomplete="clearPage('goalFrm', true);" />
	</h:form>
	
	<script>
		$("#startDate, #endDate").omCalendar();
		$("div.select-div").select();
		
		if($("#operate\\:goalId").val()) {
			initEdit();			
		}
		
		function initEdit() {
			$("#type").selectVal($("#operate\\:type").val());
			$("#priority").selectVal($("#operate\\:priority").val());
		}
		
		function saveGoal() {
			$("#operate\\:type").val($("#type").selectVal());
			$("#operate\\:priority").val($("#priority").selectVal());
			
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>