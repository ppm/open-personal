<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="saveHabit();return false;">保存</a>
		<a href="#" onclick="clearPage('hbtFrm');return false;">返回</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">顺序</td>
				<td class="edit-whole" colspan="3">
					<h:inputText value="#{habitBean.habit.sn}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">习惯</td>
				<td class="edit-whole" colspan="3">
					<h:inputText value="#{habitBean.habit.name}" styleClass="edit-require" />
				</td>
			</tr>
		</table>
		
		<a4j:commandLink id="save" action="#{habitBean.saveHabit}" oncomplete="clearPage('hbtFrm', true);" />
	</h:form>
	
	<script>
		function saveHabit() {
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>