<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="saveCatagory();return false;">保存</a>
		<a href="#" onclick="clearPage('catFrm');return false;">返回</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">顺序</td>
				<td class="edit-whole" colspan="3">
					<h:inputText value="#{siteBean.catagory.sn}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">分类名称</td>
				<td class="edit-whole" colspan="3">
					<h:inputText value="#{siteBean.catagory.name}" styleClass="edit-require" />
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="catagoryId" value="${param.catagoryId}" />
		<h:inputHidden id="catagoryId" value="#{siteBean.catagory.id}" />
		
		<a4j:commandLink id="save" action="#{siteBean.saveCatagory}" oncomplete="clearPage('catFrm', true);" />
	</h:form>
	
	<script>
		function saveCatagory() {
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>