<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/app.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
</head>

<body>
	<div id="calculator" class="app">
		<img src="<c:url value="/images/app/calculator.png" />" />
		<div class="app-name">计算器</div>
	</div>
	
	<div id="lock" class="app">
		<img src="<c:url value="/images/app/lock.png" />" />
		<div class="app-name">CSS3时钟</div>
	</div>
	
	<div id="course" class="app">
		<img src="<c:url value="/images/app/course.png" />" />
		<div class="app-name">课程表</div>
	</div>
	
	<script>
		$("div.app").click(function() {
			var id = $(this).attr("id");
			buildPage("appFrm", basePath + "app/" + id + "/" + id + ".jsf");
		});
	</script>
</body>
</html>
