<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="changePassword();return false;">保存</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">原始密码</td>
				<td class="edit-whole" colspan="3"><input id="pwd1" type="password" class="edit-require" /></td>
			</tr>
			<tr>
				<td class="edit-label">新密码</td>
				<td class="edit-whole" colspan="3"><input id="pwd2" type="password" class="edit-require" name="password" /></td>
			</tr>
			<tr>
				<td class="edit-label">确认密码</td>
				<td class="edit-whole" colspan="3"><input id="pwd3" type="password" class="edit-require" /></td>
			</tr>
		</table>
		
		<a4j:commandLink id="save" action="#{userBean.changePassword}" oncomplete="alert('修改密码成功！');" />
	</h:form>
	
	<h:inputHidden id="originPwd" value="#{userBean.originPassword}" />
	
	<script>
		function changePassword() {
			
			if($("#pwd1").val() !=  $("#originPwd").val()) {
				alert("原始密码错误！");
				return;
			}
			
			if($("#pwd2").val() != $("#pwd3").val()) {
				alert("新密码与确认密码不匹配！");
			}
			
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>