<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/editor/plugins/code/prettify.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/editor/plugins/code/prettify.js" />"></script>
	
	<style>
		#content {line-height: 25px;overflow: auto;font-family: Georgia, serif;}
	</style>
</head>

<body style="background-color: #DBE2EB;">
	<div class="menu-title">
		<a href="#" onclick="clearPage('knowEntityFrm');return false;">返回</a>
		<a href="#" onclick="editKnow();return false;">编辑</a>
	</div>
	
	<div id="content">
		<div style="text-align: center;font-size: 15px;font-weight: bold;padding: 10px;">
			<h:outputText value="#{knowBean.know.name}" />
		</div>
		
		<div style="padding: 0 10px;">
			<h:outputText value="#{knowBean.know.content}" escape="false" />
		</div>
	</div>
	
	<script>
		prettyPrint();
		$("#content").height(getViewH());
	
		function editKnow() {
			buildPage("knowFrm", basePath + "pages/knowledge/knowAdd.jsf?knowId=${param.knowId}");
		}
		
		function refresh() {
			window.location = window.location;
		}
	</script>
</body>
</html>
</f:view>