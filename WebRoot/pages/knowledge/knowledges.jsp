<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.widget.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.mouse.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.table.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="edit();return false;">编辑知识分类</a>
	</div>
	
	<h:form id="operate">
		<table id="knowledgeTab" class="list-table" cellpadding="0" cellspacing="0">
			<tr>
				<th width="50px">序号</th>
				<th width="400px">名称</th>
				<th width="150px">创建时间</th>
			</tr>
			
			<a4j:repeat value="#{knowledgeBean.knowledges}" var="item" rowKeyVar="row">
			<tr id="<h:outputText value="#{item.id}" />" ondblclick="openKnowledge($(this));">
				<td class="sn"><h:outputText value="#{row + 1}" /></td>
				<td><span class="auto-link"><a href="#" onclick="$(this).closest('tr').dblclick();return false;"><h:outputText value="#{item.name}" /></a></span></td>
				<td><h:outputText value="#{item.createTime}" converter="TimeConverter" /></td>
			</tr>
			</a4j:repeat>
		</table>
		
		<a4j:commandLink id="refresh" reRender="operate" oncomplete="initTab();" />
	</h:form>
	
	<script>
		var tableMain;
		initTab();
		
		function initTab() {
			tableMain = $("#knowledgeTab").table();			
		}
		
		function openKnowledge($tr) {
			buildPage("kngFrm", basePath + "pages/knowledge/knows.jsf?knowledgeId=" + $tr.attr("id"));
		}
	
		function edit() {
			buildPage("kngFrm", basePath + "pages/knowledge/knowledgeEdit.jsf");
		}
		
		function refresh() {
			$("#operate\\:refresh").click();
		}
	</script>
</body>
</html>
</f:view>